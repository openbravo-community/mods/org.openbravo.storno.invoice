/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2011 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 *************************************************************************
 */
package org.openbravo.storno.invoice.modulescript;

import org.openbravo.database.ConnectionProvider;
import org.openbravo.modulescript.ModuleScript;

public class SetupAccounting extends ModuleScript {
  
  @Override
  public void execute() {
    try {
      ConnectionProvider cp = getConnectionProvider();
      
      // For all accounting schemas insert acccounting schema table document type for c_invoice
      SetupAccountingData[] data = SetupAccountingData.selectInvoiceAcctSchemaTables(cp);
      for (SetupAccountingData opc : data) {
        if (!SetupAccountingData.existSchemaTableDoc(cp, opc.cAcctschemaTableId, "STORNO_ARS")) {
          SetupAccountingData.insertSchemaTableDoc(cp, opc.cAcctschemaTableId, opc.adClientId, "0", "STORNO_ARS");
        }
        if (!SetupAccountingData.existSchemaTableDoc(cp, opc.cAcctschemaTableId, "STORNO_APS")) {
          SetupAccountingData.insertSchemaTableDoc(cp, opc.cAcctschemaTableId, opc.adClientId, "0", "STORNO_APS");
        }
      }
      
      // Open period control for Storno docbasetypes STORNO_ARS and STORNO_APS
      SetupAccountingData.insertPeriodControl(cp);
      
      } catch (Exception e) {
        handleError(e);
      }
  }
}
