//Sqlc generated V1.O00-1
package org.openbravo.storno.invoice.modulescript;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

class SetupAccountingData implements FieldProvider {
static Logger log4j = Logger.getLogger(SetupAccountingData.class);
  private String InitRecordNumber="0";
  public String cAcctschemaTableId;
  public String adClientId;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("c_acctschema_table_id") || fieldName.equals("cAcctschemaTableId"))
      return cAcctschemaTableId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static SetupAccountingData[] selectInvoiceAcctSchemaTables(ConnectionProvider connectionProvider)    throws ServletException {
    return selectInvoiceAcctSchemaTables(connectionProvider, 0, 0);
  }

  public static SetupAccountingData[] selectInvoiceAcctSchemaTables(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT c_acctschema_table_id, ad_client_id" +
      "      FROM c_acctschema_table" +
      "      WHERE ad_table_id = '318'";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        SetupAccountingData objectSetupAccountingData = new SetupAccountingData();
        objectSetupAccountingData.cAcctschemaTableId = UtilSql.getValue(result, "c_acctschema_table_id");
        objectSetupAccountingData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectSetupAccountingData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectSetupAccountingData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    SetupAccountingData objectSetupAccountingData[] = new SetupAccountingData[vector.size()];
    vector.copyInto(objectSetupAccountingData);
    return(objectSetupAccountingData);
  }

  public static int insertPeriodControl(ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      INSERT INTO c_periodcontrol(" +
      "            c_periodcontrol_id, ad_client_id, ad_org_id, isactive, created," +
      "            createdby, updated, updatedby, c_period_id, docbasetype, periodstatus," +
      "            periodaction, processing)" +
      "      select get_uuid(), c_period.ad_client_id as ad_client_id, a.ad_org_id, 'Y', now()," +
      "      '100', now(), '100', c_period.c_period_id, b.storno_docbasetype, coalesce(max(periodstatus),'C') as status," +
      "      'N', 'N'" +
      "      from c_year, c_period left join c_periodcontrol on c_period.c_period_id = c_periodcontrol.c_period_id," +
      "           (select ad_org_id from ad_org where isperiodcontrolallowed = 'Y') a," +
      "           (select to_char('STORNO_ARS') as storno_docbasetype from dual union select to_char('STORNO_APS') as storno_docbasetype from dual) b" +
      "      where c_period.c_year_id = c_year.c_year_id" +
      "            and c_calendar_id = (select c_calendar_id from ad_org" +
      "                                 where ad_org_id = ad_org_getcalendarowner(a.ad_org_id))" +
      "            and not exists (select 1 from c_periodcontrol" +
      "                            where c_periodcontrol.c_period_id = c_period.c_period_id" +
      "                                  and c_periodcontrol.docbasetype in ('STORNO_ARS', 'STORNO_APS')" +
      "                                  and c_periodcontrol.ad_org_id = a.ad_org_id)" +
      "      group by c_period.ad_client_id, c_period.c_period_id, a.ad_org_id, b.storno_docbasetype";

    int updateCount = 0;
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int insertSchemaTableDoc(ConnectionProvider connectionProvider, String acctSchemaTableId, String clientId, String orgId, String docbasetype)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      INSERT INTO c_acctschema_table_doctype (" +
      "            c_acctschema_table_doctype_id, c_acctschema_table_id," +
      "            ad_client_id, ad_org_id," +
      "            isactive, created, createdby, updated, updatedby, " +
      "            allownegative, docbasetype, ad_createfact_template_id)" +
      "      VALUES ( get_uuid(), ?," +
      "               ?, ?," +
      "               'Y', now(), '100', now(), '100'," +
      "               'Y', ?, 'A5FF3BD6D1DC4035A0603D1356219ED5')";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctSchemaTableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docbasetype);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static boolean existSchemaTableDoc(ConnectionProvider connectionProvider, String acctSchemaTableId, String docbasetype)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT c_acctschema_table_doctype_id" +
      "        FROM c_acctschema_table_doctype" +
      "        WHERE ad_createfact_template_id = 'A5FF3BD6D1DC4035A0603D1356219ED5'" +
      "              AND c_acctschema_table_id = ?" +
      "              AND docbasetype = ?";

    ResultSet result;
    boolean boolReturn = false;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctSchemaTableId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docbasetype);

      result = st.executeQuery();
      if(result.next()) {
        boolReturn = !UtilSql.getValue(result, "c_acctschema_table_doctype_id").equals("0");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(boolReturn);
  }
}
